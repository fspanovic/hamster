import pygame

#Setting window size
WIDTH = 1024
HEIGHT = 600

pygame.font.init()
screen = pygame.display.set_mode((WIDTH, HEIGHT))

#Setting title of the window
pygame.display.set_caption("Presentations")

#Colors
WHITE   = (255, 255, 255)
BLACK   = (0, 0, 0)

#Game clock
clock = pygame.time.Clock()

#Background Images
def load_image(image_name):
    image = pygame.image.load(image_name)
    image = pygame.transform.scale(image, (WIDTH, HEIGHT))
    image.set_alpha(128)
    return image
def increament_image_index(index):
    index += 1
    if index > 5:
        index = 1
    return index

#Slide Text
slide_title = ["Naslov1", "Naslov2", "Naslov3", "Naslov4", "Naslov5"]
slide_text = [
    "Hala Madrid.",
    "Ovaj predmet nema smisla.",
    "Mi smo strucni studij.",
    "blablablabla.",
    "random tekst."
]

def update_slider_text(index):
    font = pygame.font.SysFont("Arial", 22)
    text = font.render(str(image_index), False, BLACK)
    return text

def truncate_text(text, font, max_width):
    text_len = len(text)
    return_text = text
    font_size = font.size(text)[0]
    cut_len = 0
    max_split = 0
    is_done = 1

    while text_len > max_width:
        max_split += 1
        splited_text = text.rsplit(None, max_split)[0]
        if return_text == splited_text:
            cut_len += 1
            return_text = splited_text[:-cut_len]
        else:
            return_text = splited_text
        font_size = font.size(return_text)[0]
        text_len = len(return_text)
        is_done = 0
    return text_len, is_done, return_text

def wrap_line(text, font, maxwidth): 
    done=0                      
    wrapped=[]                  
                               
    while not done:             
        nl, done, stext=truncate_text(text, font, maxwidth) 
        wrapped.append(stext.strip())                  
        text=text[nl:]                                 
    return wrapped

def blit_text(surface, text, pos, font, color=pygame.Color('black')):
    words = [word.split(' ') for word in text.splitlines()] 
    space = font.size(' ')[0]
    max_width, max_height = surface.get_size()
    x, y = pos
    for line in words:
        for word in line:
            word_surface = font.render(word, 0, color)
            word_width, word_height = word_surface.get_size()
            if x + word_width >= max_width:
                x = pos[0]
                y += word_height
            surface.blit(word_surface, (x, y))
            x += word_width + space
        x = pos[0]
        y += word_height

def show_slide_text(index):
    font = pygame.font.SysFont("Arial", 35)
    title = font.render(slide_title[index], False, BLACK)
    screen.blit(title, (WIDTH / 2, 10))

    font = pygame.font.SysFont("Arial", 25)
    max_width = WIDTH - 90
    wraped_text = wrap_line(str(slide_text[index]), font, max_width)

    string = str(wraped_text).replace("']", "")
    string = string.replace("['", "")
    blit_text(screen, string, (20, 50), font)
    return title

image_index = 1
current_image = load_image("slika-" + str(image_index) + ".jpg")

#Game Loop
is_done = False
game_state = "playing"
wait_counter = 5000

while not is_done:
    #Event Loop
    for event in pygame.event.get():
        if event.type == pygame.QUIT:
            is_done = True
            break
        elif event.type == pygame.KEYDOWN:
            if event.key == pygame.K_SPACE:
                image_index = increament_image_index(image_index)
                current_image = load_image("slika-" + str(image_index) + ".jpg")
                game_state = "playing"
    

    if game_state == "playing":
        screen.fill((255, 255, 255))

        screen.blit(current_image, (0, 0))

        show_slide_text(image_index - 1)
        slider_text = update_slider_text(image_index - 1)
        screen.blit(slider_text, (WIDTH - 25, HEIGHT - 25))
        
        wait_counter = 5000
        game_state = "wainting"
    
    elif game_state == "wainting":
        wait_counter -= clock.get_time()
        if wait_counter <= 0:
            image_index = increament_image_index(image_index)
            current_image = load_image("slika-" + str(image_index) + ".jpg")
            game_state = "playing"

    pygame.display.flip()

    #FPS on 60Hz
    clock.tick(60)